import os
import shutil
import tarfile
import time
from pathlib import Path
import cv2
import numpy as np
import tensorflow as tf
import urllib3
from object_detection.utils import label_map_util
from object_detection.utils import ops as utils_ops
from object_detection.utils import visualization_utils as vis_util

MODEL_URL = "http://download.tensorflow.org/models/object_detection/"
LABELS_URL = "https://raw.githubusercontent.com/tensorflow/models/master/research/object_detection/data/mscoco_label_map.pbtxt"
PATH_TO_LABELS = "/home/ryax/mscoco_label_map.pbtxt"
TMP_DIR = "/home/ryax"
IMAGE_EXT = [
    ".bmp",
    ".pbm",
    ".pgm",
    ".ppm",
    ".sr",
    ".ras",
    ".jpeg",
    ".jpg",
    ".jpe",
    ".jp2",
    ".tiff",
    ".tif",
    ".png",
]


def download_data(model_name):
    http = urllib3.PoolManager(cert_reqs='CERT_NONE')
    if not os.path.isfile(PATH_TO_LABELS):
        # Downloading label map
        r = http.request("GET", LABELS_URL)
        if r.status == 200:
            f = open(PATH_TO_LABELS, "wb")
            f.write(r.data)
            f.close()
    if not os.path.isdir(model_name):
        # Downloading model
        MODEL_DATA = model_name + ".tar.gz"
        print(f"Dowloading model {MODEL_URL + MODEL_DATA}")
        r = http.request("GET", MODEL_URL + MODEL_DATA)
        if r.status == 200:
            TF_MODEL_PATH = TMP_DIR + "/" + MODEL_DATA
            f = open(TF_MODEL_PATH, "wb")
            f.write(r.data)
            f.close()
            tar_file = tarfile.open(TF_MODEL_PATH)
            for file in tar_file.getmembers():
                file_name = os.path.basename(file.name)
                if "frozen_inference_graph.pb" in file_name:
                    tar_file.extract(file, TMP_DIR)
                    print(f"Extracted file {file.name} on directory {TMP_DIR}")
                    os.remove(TF_MODEL_PATH)
        else:
            raise Exception(f"Invalid model name: {model_name}")


def load_data(model_name):
    print("Loading TensorFlow model...")
    # Load a (frozen) Tensorflow model into memory.
    frozen_graph = os.path.join(
        os.path.join(TMP_DIR, model_name), "frozen_inference_graph.pb"
    )
    print(f"Checking for model in {frozen_graph}")
    if os.path.isfile(frozen_graph):
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.compat.v1.GraphDef()
            with tf.compat.v1.gfile.GFile(frozen_graph, "rb") as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name="")
        # Loading label map
        category_index = label_map_util.create_category_index_from_labelmap(
            PATH_TO_LABELS, use_display_name=True
        )
        return detection_graph, category_index
    else:
        raise Exception(f"Cannot find model: {model_name}")


def find_files(path: Path) -> list:
    imageList = []
    fileList = os.listdir(path)
    for file in fileList:
        _, ext = os.path.splitext(file)
        if ext in IMAGE_EXT:
            imageList.append(path / file)
    imageList.sort()
    return imageList


def load_tensors():
    # Get handles to input and output tensors
    ops = tf.compat.v1.get_default_graph().get_operations()
    all_tensor_names = {output.name for op in ops for output in op.outputs}
    tensor_dict = {}
    for key in [
        "num_detections",
        "detection_boxes",
        "detection_scores",
        "detection_classes",
        "detection_masks",
    ]:
        tensor_name = key + ":0"
        if tensor_name in all_tensor_names:
            tensor_dict[key] = tf.compat.v1.get_default_graph().get_tensor_by_name(tensor_name)
    if "detection_masks" in tensor_dict:
        # The following processing is only for single image
        detection_boxes = tf.squeeze(tensor_dict["detection_boxes"], [0])
        detection_masks = tf.squeeze(tensor_dict["detection_masks"], [0])
        # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
        real_num_detection = tf.cast(tensor_dict["num_detections"][0], tf.int32)
        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        detection_masks = tf.slice(
            detection_masks, [0, 0, 0], [real_num_detection, -1, -1]
        )
    else:
        detection_boxes = None
        detection_masks = None
    return tensor_dict, detection_masks, detection_boxes


def run_inference_for_single_image(
        image, session, tensor_dict, detection_masks, detection_boxes
):
    if "detection_masks" in tensor_dict:
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
            detection_masks, detection_boxes, image.shape[1], image.shape[2]
        )
        detection_masks_reframed = tf.cast(
            tf.greater(detection_masks_reframed, 0.5), tf.uint8
        )
        # Follow the convention by adding back the batch dimension
        tensor_dict["detection_masks"] = tf.expand_dims(detection_masks_reframed, 0)
    image_tensor = tf.compat.v1.get_default_graph().get_tensor_by_name("image_tensor:0")
    # Run inference
    output_dict = session.run(tensor_dict, feed_dict={image_tensor: image})
    # all outputs are float32 numpy arrays, so convert types as appropriate
    output_dict["num_detections"] = int(output_dict["num_detections"][0])
    output_dict["detection_classes"] = output_dict["detection_classes"][0].astype(
        np.int64
    )
    output_dict["detection_boxes"] = output_dict["detection_boxes"][0]
    output_dict["detection_scores"] = output_dict["detection_scores"][0]
    if "detection_masks" in output_dict:
        output_dict["detection_masks"] = output_dict["detection_masks"][0]
    return output_dict


def handle(req):
    slurm_rank = 0
    slurm_size = 1
    try:
        slurm_rank = int(os.environ["SLURM_PROCID"])
        slurm_size = int(os.environ["SLURM_NPROCS"])
    except KeyError:
        pass

    print(f"Request =======> {req}")
    output_dir = Path("/iodir/result_video")
    barrier_file = Path("/iodir/green_light.txt")
    # Only rank 0 download models if needed, others will
    if slurm_rank == 0:
        download_data(req["model"])
        if not output_dir.is_dir():
            os.mkdir(output_dir)
        with open(barrier_file, "w") as gofile:
            gofile.write("this is the way")
    else:
        barrier_sleep = 1
        barrier_tries = 1000
        while not barrier_file.is_file() and barrier_tries > 0:
            barrier_tries -= 1
            time.sleep(barrier_sleep)

    # Load model
    detection_graph, category_index = load_data(req["model"])
    with detection_graph.as_default():
        with tf.compat.v1.Session() as sess:
            tensor_dict, detection_masks, detection_boxes = load_tensors()
            # Finding image files in directory
            images_path = Path(req["images"])
            images_list = find_files(images_path)
            image_size = len(images_list) // slurm_size
            image_first_index = image_size * slurm_rank
            if slurm_rank == slurm_size - 1:
                image_size += len(images_list) % slurm_size

            # Start detection for each image
            print(
                f" Rank {slurm_rank} : Working images {images_list[image_first_index]} to {images_list[(image_size + image_first_index) - 1]}")
            for file_name in images_list[image_first_index:(image_size + image_first_index)]:
                img = cv2.imread(str(file_name))
                output_dict = run_inference_for_single_image(
                    np.expand_dims(img, axis=0),
                    sess,
                    tensor_dict,
                    detection_masks,
                    detection_boxes,
                )
                # Draw boxes
                vis_util.visualize_boxes_and_labels_on_image_array(
                    img,
                    output_dict["detection_boxes"],
                    output_dict["detection_classes"],
                    output_dict["detection_scores"],
                    category_index,
                    instance_masks=output_dict.get("detection_masks"),
                    use_normalized_coordinates=True,
                    line_thickness=8,
                )
                # Save tagged image
                _, name = os.path.split(file_name)
                name, ext = os.path.splitext(name)
                tagged_image = output_dir / f"{name}_tagged{ext}"
                cv2.imwrite(str(tagged_image), img)

            # if there are more workers need to wait for others to finish
            if slurm_rank == 0 and slurm_size > 1:
                retries = 10000
                retry_interval = 1
                # wait until the number tagged_images match the full set of input images
                while len(os.listdir(output_dir)) < len(images_list) and retries > 0:
                    print(
                        f"Rank {slurm_rank} : Waiting for result files, have {len(os.listdir(output_dir))} out of {len(images_list)}")
                    retries -= 1
                    time.sleep(retry_interval)
                    print(f"Rank {slurm_rank} : Retrying {retries}")
                if len(os.listdir(output_dir)) == len(images_list):
                    print(
                        f"Rank {slurm_rank} : Finished fetching results, have {len(os.listdir(output_dir))} out of {len(images_list)}")
                else:
                    print(
                        f"Rank {slurm_rank} : ERROR could not fetch all FILES ERROR, have {len(os.listdir(output_dir))} out of {len(images_list)}")
            if slurm_rank == 0:
                shutil.copy(images_path / "info.json", output_dir / "info.json")

    if slurm_rank == 0:
        return {"tagged_images": str(output_dir)}
    else:
        empty_dir = Path("/tmp/empty")
        if not Path(empty_dir).is_dir():
            os.mkdir(empty_dir)
        return {"tagged_images": "/tmp/empty"}


if __name__ == "__main__":
    handle({
        "model": "ssdlite_mobilenet_v2_coco_2018_05_09",
        "images": "/tmp/output",
    })
