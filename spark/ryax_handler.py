import json
import os
import tempfile
import time
from pathlib import Path
import subprocess


# Get spark home from the Nix store
for spark_path in Path("/nix/store").glob("**/*spark-*"):
    print(f"Found spark installed at {spark_path}")
    os.environ['SPARK_HOME'] = str(spark_path)
    break
SPARK_HOME = os.environ['SPARK_HOME']
if SPARK_HOME is not None:
    print(f"SPARK_HOME={SPARK_HOME}")
else:
    print("ERROR SPARK_HOME environment variable not set!!!")

os.environ["SPARK_LOG_DIR"] = "/tmp/spark-logs"
os.environ["SPARK_WORKER_DIR"] = "/tmp/spark-worker"
os.environ["SPARK_SSH_OPTS"] = "-p 2222 -o StrictHostKeyChecking=no"


with open('/etc/hostname') as f:
    pod_name = f.read()
print(f"Pod HOSTNAME is ========> {pod_name}")
os.environ["SPARK_LOCAL_HOSTNAME"] = pod_name

def execute(cmd):
    popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
    for stdout_line in iter(popen.stdout.readline, ""):
        yield stdout_line
    popen.stdout.close()
    return_code = popen.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd)

def handle(ryax_input):

    print("====== Dumping local environment variable parameters  ======")
    for envar in os.environ:
        print(f"{envar}={os.environ[envar]}")

    print("====== Dumping ryax_input dict  ======")
    print(json.dumps(ryax_input, indent=2))


    print("====== Spark application parameters  ======")

    # The application itself
    application_jar = ryax_input.get("application_jar")
    if application_jar is None:
        print(f"WARNING: application_jar file not provided")
        for examples_jar_path in Path(SPARK_HOME).glob("**/*spark-examples*.jar"):
            print("Trying to find spark examples jar file...")
            spark_examples_jar_path = str(examples_jar_path)
            print(f"Using examples jar file as the application in {spark_examples_jar_path} ...")
            break
    if not Path(application_jar).exists():
        print(f"ERROR: application jar file {spark_examples_jar_path} not found!")
        exit(1)

    print(f"application_jar={application_jar}")

    application_main_class = ryax_input.get("application_main_class")
    if application_main_class is None:
        print(f"ERROR: application_main_class not specified")
        exit(1)
    print(f"application_main_class={application_main_class}")

    # A service account set on the kubernetes cluster to allow modifications
    # from driver pod.
    spark_service_account = os.environ.get("KUBERNETES_SERVICE_ACCOUNT_NAME")
    if spark_service_account is None:
        print(f"ERROR: spark_service_account not set, did you provide service_account_name parameter of kubernetes addon?")
        exit(1)
    print(f"spark_service_account={spark_service_account}")

    # Application arguments, can be empty so it is optional
    application_args = ryax_input.get("application_args")
    if application_args is None:
        print(f"WARNING: application_args not specified, using empty list...")
    application_args_list = application_args.split()
    print(f"application_args={application_args}")

    # Number of instances of executors to spawn, default is 4.
    spark_executor_instances = ryax_input.get("spark_executor_instances")
    if spark_executor_instances is None:
        spark_executor_instances = 4
        print(f"WARNING: spark_executor_instances not specified, using default {spark_executor_instances}...")
    print(f"spark_executor_instances={spark_executor_instances}...")

    # A pod template is optional, it can be used to target specific
    # nodes or finetune other configuration parameters.
    spark_executor_pod_template = ryax_input.get("spark_executor_pod_template")
    if spark_executor_pod_template is None or not Path(spark_executor_pod_template).exists():
        spark_executor_pod_template = "/data/executor-pod-template.yaml"
        print(f"WARNING: spark_executor_pod_template not specified, using default {spark_executor_pod_template}...")
    print(f"spark_executor_pod_template={spark_executor_pod_template}...")

    # Memory allocated per spark executor pod, default is 2 GiB
    spark_executor_mem = ryax_input.get("spark_executor_mem")
    if spark_executor_mem is None:
        spark_executor_mem = "2g"
        print(f"WARNING: spark_executor_mem not specified, using default {spark_executor_mem}...")
    print(f"spark_executor_mem={spark_executor_mem}...")

    # Cpu allocated per spark executor pod, default is 1
    spark_executor_cores = ryax_input.get("spark_executor_cores")
    if spark_executor_cores is None:
        spark_executor_cores = "1"
        print(f"WARNING: spark_executor_cores not specified, using default {spark_executor_cores}...")
    print(f"spark_executor_cores={spark_executor_cores}...")

    # CURRENT_IMAGE_URL environment variable is set by ryax,
    # please do not remove spark-entrypoint.sh or
    # this image won't be able to run as a spark executor.
    executor_image = os.environ.get("CURRENT_IMAGE_URL")
    if executor_image is None:
        print(f"ERROR: image URL not set for this action, please contact Ryax support!")
        exit(1)

    # Get the headless service_name from the addon parameters
    # This is the parameter service_name of kubernetes addon
    # Ryax will automatically the label below to your pod
    # ryax.tech/kubernetes-service=service_name
    spark_headless_service = os.environ.get("KUBERNETES_SERVICE_NAME")
    print(f"spark_headless_service={spark_headless_service}")
    if spark_headless_service is None:
        print(f"ERROR: spark_headless_service not set, did you fill the service_name parameter of the kubernetes addon?")
        exit(1)

    spark_service_port = int(float(os.environ.get("KUBERNETES_SERVICE_PORT")))
    if spark_service_port is None or spark_service_port == "":
        spark_service_port = 4041
        print(f"ERROR: spark_service_port not set, using default {spark_service_port}")
    print(f"spark_service_port={spark_service_port}...")

    print("=====================> <===================")

    submit_bash_cmd = [
        "spark-submit",
        "--master",
        "k8s://https://kubernetes.default:443",
        "--deploy-mode",
        "client",

        # Enable Garbage collection of pods
        "--conf",
        f"spark.kubernetes.driver.pod.name={pod_name}",

        # Use Ryax actions namespace ryaxns-execs
        "--conf",
        "spark.kubernetes.namespace=ryaxns-execs",

        # Necessary to pull images from ryax registry
        "--conf",
        "spark.kubernetes.container.image.pullSecrets=ryax-registry-creds-secret",

        # Use the input specified template or the one hardcoded is case it is absent
        "--conf",
        f"spark.kubernetes.executor.podTemplateFile={spark_executor_pod_template}",

        # Image to run for the executors
        "--conf",
        f"spark.kubernetes.container.image={executor_image}",

        # Amount of cores requested per executor pod
        "--conf",
        f"spark.kubernetes.executor.request.cores={spark_executor_cores}",

        # Amount of memory required per executor pod
        "--conf",
        f"spark.executor.memory={spark_executor_mem}",

        # Use a headless service called sparkdriver to reach driver pod (current pod)
        "--conf",
        f"spark.driver.host={spark_headless_service}",

        # Default port and bind parameters
        "--conf",
        f"spark.driver.port={spark_service_port}",
        "--conf",
        "spark.driver.bindAddress=0.0.0.0",

        # Amount of executor pods to run
        "--conf",
        f"spark.executor.instances={spark_executor_instances}",

        # Require a service account with read,write,list on ryaxns-execs
        # see README.md for details
        "--conf",
        f"spark.kubernetes.authenticate.driver.serviceAccountName={spark_service_account}",

        # The jar file Main class, if you don't know try Main
        "--class",
        application_main_class,

        # Finally the applicaiton with parameter n, number of iterations
        f"file://{application_jar}",
        *application_args_list,
    ]

    # Call spark submit write stdout as the output file
    print("Running command for spark-submit...")
    for it in submit_bash_cmd:
        print(it, end=" ")
    print("")

    # All data to retrieve must go to spark_tmp_output
    spark_tmp_output = tempfile.mkdtemp()

    with open(f"{spark_tmp_output}/spark-submit.log", "w") as output_logs:
        start = time.time()
        for cmd_log_line in execute(submit_bash_cmd):
            print(cmd_log_line, end="")
            output_logs.write(cmd_log_line)
        total = time.time() - start
        print(f"Total time on spark submit ======> {total}")
        output_logs.write(f"Total time on spark submit ======> {total}")

    return { "spark_app_output":  spark_tmp_output }


if __name__ == "__main__":
    handle({
        "application_jar": "", # full path of application
        "application_args": "5000", #space separated list of application parameters
        "spark_service": "sparkpidriver", # see README.md
        "spark_executor_instances": "2", # number of executor pods to spawn
        "spark_executor_mem": "512m", # amount of memory allocated per executor pod
        "spark_executor_cores": "500m", # amount of vcores allocated per executor
    })
