
# Ryax Spark Tutorial

We guide you through launching
a spark submit command that act in 
cluster mode, so it becomes
spark driver and master on the current action pod. This will
allow Ryax to seamlessly run the spark application and
spawn as many executors you need.

Ryax action pod will be the spark driver. This 
will spam multiple spark executors across kubernetes. Several input
parameters are exposed but optional, such as executor pod memory/cpu,
amount of executors.

To enable this pod to enact on kube we must
use the kubernetes addon that
enables ryax actions to specify: `service_account_name`, `labels`, and `service_name`.
Other kubernetes addon parameters can be left blank for this purpose.
NOTE, the cluster administrator *must* create a service account and
give you that `service account` name so you can instantiate the
ryax spark action properly.


## Pre-requirements

### serviceAccount

**Disclaimer**: the creation of serviceAccountName must be
made by an administrator on kubernetes. This is step
is to be performed by a kuberntes system administrator, someone
that has full access to `kubectl` and can create the serviceAccount resource on
ryaxns-execs namespace.

This action will require kubernetes addon to
associate with a serviceAccountName that can list/delete/create on
services, pods, persistentvolumeclaims, and configmaps kubernetes' resources.
This will grant kube access required for spark to spam the workers across
desired kubernetes nodes.
The serviceAccount must  be created beforehand in namespace `ryaxns-execs`
with a serviceAccountName that is later used with the kubernetes addon.
To edit the kubernetes addon serviceAccountName one can either edit the
workflow on Ryax UI before deployment or provide an initial value in `ryax_metadata.yaml`.

To create the service account with the right permissions you can use a sample
yaml in this project `spark-rbac.yaml`. To apply that file you are required to
have administration rights on the cluster to issue a `kubectl apply` command
as below.

```shell
kubectl apply -f spark-rbac.yaml
```

To check if the service account has the correct permission you can run the command
below.

```shell
for op in "list" "delete" "create" "update"
do
    for resource in "pods" "services" "configMaps" "persistentVolumeClaims"
    do
      echo "Checking for $op on $resource =====>" `kubectl auth can-i $op $resource --as=system:serviceaccount:ryaxns-execs:spark -n ryaxns-execs`
    done
done
```

Note to azure clusters, if you get the response `no Azure has no opinion for
this user` this means the rbac is not working, double check the namespace
in all resource and reapply `spark-rbac.yaml`.

### Tolerations [Optional]

Sometimes we want to use specific nodes. Like for bebida that will make
nodes available that use the best effort policy. In this case we
can make a bebida aware action by selecting the nodes where the spark
executors will run.

To select nodes where to schedulle, we can edit `exporter-pod-template.yaml`.
This file overrides toleration parameters and will make the executor.

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    ryax: spark-executor
spec:
# The pod template starts here
  containers:
    - command:
      - bash
      - /data/spark-entrypoint.sh
  # If you want the pods to execute only in nodes with label: bebida=hpc
  # uncomment below
  tolerations:
    - effect: NoSchedule
      key: bebida
      operator: Equal
      value: hpc
# The pod template ends here
```

The template state by default the toleration of taint `spark=bebida:NoSchedule`, so nodes
should receive the taint like in the example below:

```shell
kubectl taint nodes bebidanode spark=bebida:NoSchedule
```

# Running on Ryax

## Code modifications


## Configure

Only 2 parameters required for the addon kubernetes:

* Set the `service account` parameter to a given, example `spark`
* Set `service name`, example `sparkpi` 


Set the remaining parameters to match your needs:

* `executor cores`: number of vcores per executor
* `executor memory`: memory per executor, example 512m
* `executor instances`: total executor pods to spawn
* `application jar`: spark application jar file
* `application args`: space separated list of parameters


**TIP** if you need files as parameters you will need
to modify the code, add input parameters for the files and
then add files to list.

## Pi Example

For an example use the spark examples class with the SparkPi
class.

* `application jar`: "invalid file" \# anything that does not exist will default to spark-examples_X.Y-3.X.X.jar
* `application main class`: org.apache.spark.examples.SparkPi
* `application args`: 5000
* `executor cores`: 500m
* `executor memory`: 512m
* `executor instances`: 2

For the addon parameters:

* `Pod serviceAccountName`: spark
* `Kubernetes service name`: sparkpi
* `Kubernetes service port`: 4041

The other addon parameters you may leave it blank.

    * `Pod labels`
* `Pod annotations`
* `Pod nodeSelector`

## Execute

* Deploy your workflow
* Trigger an execution
* Wait for the results (meanwhile you can check on kubernetes the pods are beign created by the spark driver)

