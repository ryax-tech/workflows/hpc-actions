import subprocess
import os


def handle(inputs: dict) -> None:
    print(inputs)

    with open("./script.sh", "r") as script_file:
        script = script_file.read()

    subprocess.run(
        script,
        shell=True,
        check=True,
        env={**inputs, **os.environ}
    )
    return {"results_dir": "/tmp"}


if __name__ == "__main__":
    handle({})
