#/usr/bin/env bash
set -x
set -u

mpirun --host localhost:$nb_process -np $nb_process $bench.$class.mpi
