
# Tested Working

Works on UCit cluster, the cluster must have openmpi version 4.6.1 for it 
to work. Otherwise, we can get a segmentation fault during the mpi calls.
Working version is `openfoam11-2.4.0-grep`.


```yaml
actionBuilder:
  chartVersion: 24.01.0-3
  values:
    resources:
      limits:
        cpu: 8
        memory: 16Gi
      requests:
        cpu: 8
        memory: 16Gi
clusterName: demo
datastore:
  pvcSize: 20Gi
domainName: ryax.io
logLevel: debug
minio:
  pvcSize: 20Gi
monitoring:
  enabled: true
rabbitmq:
  values:
    auth:
      password: KzhOgNz6NYE9
registry:
  chartVersion: 24.02.0-2
  pvcSize: 40Gi
repository:
  chartVersion: 24.02.0-1
  logLevel: debug
  resources:
    limits:
      memory: 1Gi
    requests:
      cpu: 500m
      memory: 1Gi
runner:
  chartVersion: 24.03.0-dev2
  values:
    hpcOffloading: true
    logLevel: debug
    resources:
      limits:
        cpu: 4
        memory: 8Gi
      requests:
        cpu: 4
        memory: 8Gi
storageClass: ''
studio:
  chartVersion: 24.03.0-dev1
tls:
  enabled: true
version: 24.02.0
```
