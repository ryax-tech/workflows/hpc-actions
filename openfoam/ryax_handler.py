import subprocess
import os


def handle(inputs: dict) -> None:
    print(inputs)

    with open("./script.sh", "r") as script_file:
        script = script_file.read()

    subprocess.run(
        script,
        shell=True,
        check=True,
        #env={"NUM_PROCESSORS": str(inputs["nb_cores"]), **os.environ}
        #if inputs.get("nb_cores")
        #else os.environ,
    )
    return {"results_dir": "/home/ryax/motorBike"}


if __name__ == "__main__":
    handle({})
