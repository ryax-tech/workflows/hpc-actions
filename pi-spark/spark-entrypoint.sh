#!/usr/bin/env bash

# Change working directory
mkdir -p /tmp/spark-worker
cd /tmp/spark-worker

# echo commands to the terminal output
set -ex
SPARK_HOME=(/nix/store/*-spark-*)
HADOOP_HOME=(/nix/store/*-hadoop-*)
export SPARK_WORKER_DIR="/tmp/spark-worker"

SPARK_CLASSPATH="$SPARK_CLASSPATH:${SPARK_HOME}/jars/*"
env | grep SPARK_JAVA_OPT_ | sort -t_ -k4 -n | sed 's/[^=]*=\(.*\)/\1/g' > /tmp/java_opts.txt
readarray -t SPARK_EXECUTOR_JAVA_OPTS < /tmp/java_opts.txt

# Add hadoop and spark to path
export PATH=$PATH:$SPARK_HOME/bin:$HADOOP_HOME/bin

# Fix java.lang.NoClassDefFoundError: org/slf4j/Logger
export SPARK_DIST_CLASSPATH=`hadoop classpath`

if [ -n "$SPARK_EXTRA_CLASSPATH" ]
then
  SPARK_CLASSPATH="$SPARK_CLASSPATH:$SPARK_EXTRA_CLASSPATH"
fi

if [ "$PYSPARK_MAJOR_PYTHON_VERSION" == "2" ]
then
    pyv="$(python -V 2>&1)"
    export PYTHON_VERSION="${pyv:7}"
    export PYSPARK_PYTHON="python"
    export PYSPARK_DRIVER_PYTHON="python"
elif [ "$PYSPARK_MAJOR_PYTHON_VERSION" == "3" ]
then
    pyv3="$(python3 -V 2>&1)"
    export PYTHON_VERSION="${pyv3:7}"
    export PYSPARK_PYTHON="python3"
    export PYSPARK_DRIVER_PYTHON="python3"
fi

if ! [ -z "${HADOOP_CONF_DIR+x}" ]
then
  SPARK_CLASSPATH="$HADOOP_CONF_DIR:$SPARK_CLASSPATH";
fi


case "$1" in
  driver)
    shift 1
    CMD=(
      "$SPARK_HOME/bin/spark-submit"
      --conf "spark.driver.bindAddress=$SPARK_DRIVER_BIND_ADDRESS"
      --deploy-mode client
      "$@"
    )
    ;;
  executor)
    shift 1
    CMD=(
      "/bin/java"
      "${SPARK_EXECUTOR_JAVA_OPTS[@]}"
      -Xms"$SPARK_EXECUTOR_MEMORY"
      -Xmx"$SPARK_EXECUTOR_MEMORY"
      -cp "$SPARK_CLASSPATH:$SPARK_DIST_CLASSPATH"
      org.apache.spark.executor.CoarseGrainedExecutorBackend
      --driver-url "$SPARK_DRIVER_URL"
      --executor-id "$SPARK_EXECUTOR_ID"
      --cores "$SPARK_EXECUTOR_CORES"
      --app-id "$SPARK_APPLICATION_ID"
      --hostname "$SPARK_EXECUTOR_POD_IP"
    )
    ;;
  *)
    echo "Non-spark-on-k8s command provided, proceeding in pass-through mode..."
    CMD=("$SPARK_HOME/bin/spark-submit" "$@")
    ;;
esac

exec "${CMD[@]}"

