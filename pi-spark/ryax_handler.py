import os
import time
from pathlib import Path
import subprocess

print("Setting SPARK_HOME environment variable...")

# Get spark home from the Nix store
for spark_path in Path("/nix/store").glob("**/*spark-*"):
    print(f"Found spark installed at {spark_path}")
    os.environ['SPARK_HOME'] = str(spark_path)
    print(f"SPARK_HOME={os.environ['SPARK_HOME']}")
    break
SPARK_HOME = os.environ['SPARK_HOME']
os.environ["SPARK_LOG_DIR"] = "/tmp/spark-logs"
os.environ["SPARK_WORKER_DIR"] = "/tmp/spark-worker"
os.environ["SPARK_SSH_OPTS"] = "-p 2222 -o StrictHostKeyChecking=no"

with open('/etc/hostname') as f:
    pod_name = f.read()
print(f"Pod HOSTNAME is ========> {pod_name}")
os.environ["SPARK_LOCAL_HOSTNAME"] = pod_name

# Generate keys

def handle(ryax_input):
    n = int(ryax_input["n"])
    executor_image = ryax_input.get("executor_image")
    if executor_image is None:
        executor_image = os.environ.get("CURRENT_IMAGE_URL")
    if executor_image is None:
        print(f"ERROR: executor_image not found either as input or CURRENT_IMAGE_URL environment varariable!")
        exit(1)

    executor_pod_template = ryax_input.get("executor_pod_template")

    if executor_pod_template is None or not Path(executor_pod_template).exists():
        executor_pod_template = "/data/executor-pod-template.yaml"
        print(f"Executor pod template not specified, using default from {executor_pod_template}...")

    print(f"Working for {n} iterations...")

    # Create host keys
    Path("/tmp/.ssh").mkdir(parents=True, exist_ok=True)
    print("Creating RSA host keys...")
    subprocess.Popen([
        'ssh-keygen',
        '-q',
        '-N', '""',
        '-t', 'rsa',
        '-b', '4096',
        '-f', '/tmp/.ssh/ssh_host_rsa_key'
    ], stdout=subprocess.PIPE)
    print("Creating ed25519 host keys...")
    subprocess.Popen([
        'ssh-keygen',
        '-q',
        '-N', '""',
        '-t', 'ed25519',
        '-f', '/tmp/.ssh/ssh_host_rsa_key'
    ], stdout=subprocess.PIPE)

    for ssh_path in Path("/nix/store").glob("**/*-openssh-*/bin/sshd"):
        sshd_bin = str(ssh_path)
        break

    print("Running sshd...")
    sshd_process = subprocess.Popen([
            sshd_bin,
            "-D",
            "-e",
            "-f",
            "/data/ssh/sshd_config"
        ],
        stdout=subprocess.PIPE
    )
    if sshd_process.stderr is not None:
        for line in sshd_process.stderr:
            line_str = line.decode('utf-8')
            print("STDERR==> "+line_str)
    time.sleep(1)

    spark_examples_jar_path="NOT_FOUND"
    for examples_jar_path in Path(SPARK_HOME).glob("**/*spark-examples*.jar"):
        print("Trying to find spark examples jar file...")
        spark_examples_jar_path = str(examples_jar_path)
        print(f"Found examples jar in {spark_examples_jar_path} ...")
        break

    submit_bash_cmd = [
        "spark-submit",
        "--master",
        "k8s://https://kubernetes.default:443",
        "--deploy-mode",
        "client",

        # Enable Garbage collection of pods
        "--conf",
        f"spark.kubernetes.driver.pod.name={pod_name}",

        # Use Ryax actions namespace ryaxns-execs
        "--conf",
        "spark.kubernetes.namespace=ryaxns-execs",

        # Necessary to pull images from ryax registry
        "--conf",
        "spark.kubernetes.container.image.pullSecrets=ryax-registry-creds-secret",

        # Use the input specified template or the one hardcoded is case it is absent
        "--conf",
        f"spark.kubernetes.executor.podTemplateFile={executor_pod_template}",

        # Image to run for the executors
        "--conf",
        #"spark.kubernetes.container.image=ryaxtech/spark-on-k8s:v0.1.0",
        f"spark.kubernetes.container.image={executor_image}",

        # Amount of cores requested per executor pod
        "--conf",
        "spark.kubernetes.executor.request.cores=500m",

        # Amount of memory required per executor pod
        "--conf",
        "spark.executor.memory=512m",

        # Use a headless service called sparkdriver to reach driver pod (current pod)
        "--conf",
        f"spark.driver.host=sparkpidriver",

        # Default port and bind parameters
        "--conf",
        "spark.driver.port=4041",
        "--conf",
        "spark.driver.bindAddress=0.0.0.0",

        # Call SparkPi application
        "--class",
        "org.apache.spark.examples.SparkPi",

        # Amount of executor pods to run
        "--conf",
        "spark.executor.instances=4",

        # Require a service account with read,write,list on ryaxns-execs
        # see README.md for details
        "--conf",
        "spark.kubernetes.authenticate.driver.serviceAccountName=spark",

        # Finally the applicaiton with parameter n, number of iterations
        f"file://{spark_examples_jar_path}",
        f"{n}",
    ]

    # Call spark submit write stdout as the output file
    submit_process = subprocess.Popen(submit_bash_cmd, stdout=subprocess.PIPE)
    submit_process.wait()
    if submit_process.stderr is not None:
        for line in submit_process.stderr:
            line_str = line.decode('utf-8')
            print("STDERR==> " + line_str)

    result_pi = "3.14"
    for line in submit_process.stdout:
        line_str = line.decode('utf-8')
        print("STDOUT==>" + line_str)
        if "Pi is" in line_str:
            result_pi = line_str

    return { "pi": result_pi }


if __name__ == "__main__":
    handle({
        "n": 50000,
    })
