
# Kubernetes spark parallel example

This action launches a spark submit command and act in 
cluster mode, so it becomes
spark driver and master on the current action pod. This will
allow spark to spam multiple spark executors across kubernetes. 

To enable this pod to enact on kube we can use the kubernetes addon that
enables ryax actions to specify: `service_account_name`, `labels`, and `annotations`.
In the next section, called Configurations we show the


## Configuration

### serviceAccountName

This action will require kubernetes addon to
associate with a serviceAccountName that can list/delete/create on
services, pods, and configmaps kubernetes' resources.
This will grant kube access required for spark to spam the workers across
desired kubernetes nodes.
The serviceAccount must  be created beforehand in namespace `ryaxns-execs`
with a serviceAccountName that is later used with the kubernetes addon.
To edit the kubernetes addon serviceAccountName one can either edit the
workflow on Ryax UI before deployment or provide an initial value in `ryax_metadata.yaml`.

To create the service account with the right permissions you can use a sample
yaml in this project `spark-rbac.yaml`. To apply that file you are required to
have administration rights on the cluster to issue a `kubectl apply` command
as below.

```shell
kubectl apply -f spark-rbac.yaml
```

To check if the service account has the correct permission you can run the command
below.

```shell
for op in "list" "delete" "create" "update"
do
    for resource in "pods" "services" "configMaps" "persistentVolumeClaims"
    do
      echo "Checking for $op on $resource =====>" `kubectl auth can-i $op $resource --as=system:serviceaccount:ryaxns-execs:spark -n ryaxns-execs`
    done
done
```

Note to azure clusters, if you get the responce `no Azure has no opinion for
this user` this means the rbac is not working, double check the namespace
in all resource and reapply `spark-rbac.yaml`.


### Headless service

For the executor nodes created by spark to connect it is required a headless service so executor pods see a valid name
on the network. This requires two actions, first we need to create the headless service and associate it with
specific nodes by setting a select. The file `spark-headless-service.yaml` 
is like below.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: sparkpidriver
  namespace: ryaxns-execs
spec:
  clusterIP: None
  selector:
      run: sparkpi
  ports:
    - protocol: TCP
      port: 4041
      targetPort: 4041
```

First create that headless service.

```
kubectl apply -f spark-headless-service.yaml
```

Then on the action, using the ryax interface to edit kubernetes addon parameters,
we need to specify label `run=sparkpi` that matches the selector.
Optionally, this parameter can be set to on `ryax_metadata.yaml`.


### Tolerations

Sometimes we want to use specific nodes. Like for bebida that will make
nodes available that use the best effort policy. In this case we
can make a bebida aware action by selecting the nodes where the spark
executors will run.

To select nodes where to schedulle, we can edit `exporter-pod-template.yaml`.
This file overrides toleration parameters and will make the executor.

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    ryax: spark-executor
spec:
# The pod template starts here
  containers:
    - command:
      - bash
      - /data/spark-entrypoint.sh
  # Only schedulle on bebida nodes
  tolerations:
    - effect: NoSchedule
      key: bebida
      operator: Equal
      value: hpc
# The pod template ends here
```

The template state by default the toleration of taint `spark=bebida:NoSchedule`, so nodes
should receive the taint like in the example below:

```shell
kubectl taint nodes bebidanode spark=bebida:NoSchedule
```

